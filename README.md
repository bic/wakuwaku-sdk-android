WAKU WAKU Android SDK
====================

This Android library is a facility to have a game get coupons from the WAKU WAKU service.

Usage
-----

Usage requires 4 actions on the development side:

* Add the SDK to your project.
* Add configuration requirements to `AndroidManifest.xml`.
* Initialize the SDK within the application.
* Add at least a call request for coupons.

Optionally, you can build the SDK yourself from the [repository](https://bitbucket.org/freakhill/wakuwaku-android-sdk). See the Build section.

### Add the SDK to your project

The WAKU WAKU SDK is distributed as a `JAR` file called `wakuwaku-sdk.jar`. Simply put it in your project's `libs` folder, so that it gets loaded with other libraries.

There is a dependency to the Android v4 Support Libary. Please get it directly from [Google](http://developer.android.com/tools/support-library/setup.html#libs-without-res), or from the repository:

    <wakuwaku-android-sdk path>/libs/android-support-v4.jar

This library allows the SDK to support Android down to API level 8.

### AndroidManifest.xml

In the `<manifest>` section, please add the necessary permissions, if not already declared:

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

Before the `<application` section, please make sure you have a `<uses-sdk>` clause. This clause is [necessary to get the right screen sizes](http://stackoverflow.com/questions/5078808/android-showing-wrong-screen-resolution)! For example:

    <uses-sdk android:minSdkVersion="8" />

Note: We currently support the API levels down to 8, so the only requirement is on `android:minSdkVersion`. The two other values `android:targetSdkVersion` and `android:maxSdkVersion` can be any value above or equal to 8.

### Initialization

Please add the initialization code to your application. It is best to initialize at the same time as your application itself.

Import the necessary piece of code:

    import jp.ne.wakuwaku.sdk.Wakuwaku;

Asynchronous initialization:

    final Activity activity = this;
    Wakuwaku.activateAsync(activity,
                           "[YOUR COMPANY ID]",     // See below
                           "[YOUR APP ID]",         // See below
                           false | true,            // Production mode?
                           "landscape" | "portrait" // Desired coupon orientation
                          );

Synchronous initialization:

    final Activity activity = this;
    Wakuwaku.activate(activity
                      "[YOUR COMPANY ID]",     // See below
                      "[YOUR APP ID]",         // See below
                      false | true,            // Production mode?
                      "landscape" | "portrait" // Desired coupon orientation
                     );

Sample call: `Wakuwaku.activate(activity, "mycompanyid", "myappid", false, "landscape");`

The parameters:

- YOUR COMPANY ID (a 128-char string)
- YOUR APP ID     (a UUID string)

These IDs are provided by WAKU WAKU. Please contact the company to get them.

You can also customize two parameters.

- Production mode: `true` to get real coupons, `false` for testing
                   with fake coupons as much as you want (defaults to `false`).
- Orientation of coupons: `"landscape"` or `"portrait"` (defaults to `"landscape"`).

Note: The IDs are for tracking your account and determine what we pay you at the end of each cycle. These IDs are not aimed at security.


### Call Requests for Coupon Handling

In the application, when the user has achieved some milestone (you choose!), please add a call to request a coupon.

Import the necessary piece of code:

    import jp.ne.wakuwaku.sdk.Wakuwaku;

Call Request:

    Activity hostActivity = this; // Or whatever activity the dialog starts from.
    Wakuwaku.couponDialog(hostActivity);

What it does
------------

This SDK fetches a coupon in the background, ready to get displayed when the player has achieved the target you have chosen in the game (e.g. complete a level, acquired some object, collected X gems/coins). When the achievement is completed, the SDK displays an Android Dialog that shows the coupon, and allows to send it by email, or to discard it. The email needs be entered only once.

The dialog also shows terms of use, a WAKU button that links to the service home page, and a "W" button for the upcoming Wallet application, for coupon management.

Coupon or not Coupon?
---------------------

Depending on many conditions defined by advertisers, availability, location, user demographics, and yourself, the request MAY or MAY NOT return a coupon. This call request code handles the cases as follows:

* If a coupon is available, it is shown in a dialog.
* If no coupon is available, the call is ignored and the flow returns to the caller.

Call requests are independentent, so if the user has received no coupon at some point (and for some varying reasons), she may well receive it at the next call.

Note that you control when the call is made, so you may add some logic on top of the `Wakuwaku.couponDialog/1` call, as you please.

Build the SDK
-------------

You can build the SDK yourself from the [repository](https://bitbucket.org/freakhill/wakuwaku-android-sdk).

The dependencies are:

* A working Android SDK, at least API level 8.
* Apache Ant.
* Proper configuration of your build environment (`JAVA_HOME`, etc).
* The `wakuwaku-android-sdk` repository from [Bitbucket](https://bitbucket.org/freakhill/wakuwaku-android-sdk).

When all dependencies are fulfilled, the build is a simple Ant call:

    cd <path to the repository>
    ant release

Note: Please make sure that you build from the repository's `master` branch, and from the right tag you want. By default, the repository is on the `master` branch's HEAD commit. You can check the list of tags and checkout the right version with the following commands:

    git tag
    git checkout <desired tag name>

License
-------

This SDK is distributed under the standard MIT License. Please see LICENSE.md in this folder for more information.

